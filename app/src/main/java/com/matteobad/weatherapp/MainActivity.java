package com.matteobad.weatherapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public void checkWeather(View view) {

        CheckWeatherTask weatherTask = new CheckWeatherTask(this);

        EditText city = (EditText) findViewById(R.id.cityEditText);

        try {
            weatherTask.execute(city.getText().toString()).get();
            Log.i("info", "done");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
