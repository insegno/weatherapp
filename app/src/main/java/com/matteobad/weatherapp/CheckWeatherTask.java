package com.matteobad.weatherapp;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class CheckWeatherTask extends AsyncTask<String, Void, String> {

    String endpoint = "http://api.openweathermap.org/data/2.5/weather?q=";
    String appid = "&appid=d8255e73dbd3b3bf79e8f73889c90cc0";
    TextView weatherTextView;

    CheckWeatherTask(MainActivity mainActivity) {
        weatherTextView = (TextView) mainActivity.findViewById(R.id.weatherTextView);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        try {
            JSONObject jsonObject = new JSONObject(result);
            String weatherInfo = jsonObject.getString("weather");
            JSONArray jsonWeatherArray = new JSONArray(weatherInfo);
            Log.i("json", jsonWeatherArray.toString());

            for (int i = 0; i < jsonWeatherArray.length(); i++) {
                JSONObject weatherJsonObject = jsonWeatherArray.getJSONObject(i);
                String description = weatherJsonObject.getString("description");
                weatherTextView.setText(description);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String result = "";
            Log.i("info", endpoint + params[0] + appid);
            URL url = new URL(endpoint + params[0] + appid);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            InputStream inputStream = httpURLConnection.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);

            int data = reader.read();

            while (data != -1) {
                result += (char) data;
                data = reader.read();
            }

            Log.i("info", result);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
